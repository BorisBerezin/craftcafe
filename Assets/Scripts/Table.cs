﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;

public struct Food
{
    public int Id;    
    public Image Image;
    public bool Recived;
}

public class Table : MonoBehaviour
{
    [SerializeField] private Container contaner = null;   
    public event Action OrderCompleted;

    public void PressFoodButton(int _foodId)
    {        
        GameObject _visitorObject = contaner.QueueOfVisitors.FirstVisitor;
        _visitorObject.GetComponent<Visitor>().ReciveFood(_foodId);
    }  
    
    public void OnOrderCompleted()
    {        
        OrderCompleted?.Invoke();
    }
}


