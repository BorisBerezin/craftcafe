﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorsGenerator : MonoBehaviour
{    
    [SerializeField] private Container container = null;
    [SerializeField] private float interval = 1;
    [SerializeField] private int count = 0;

    private void Start()
    {
        CraeteVisitorAfter(interval);
        SetTimer();
    }

    public void CraeteVisitorAfter(float _time)
    {
        StartCoroutine(WaitInterval(_time));
    }

    private IEnumerator WaitInterval(float _time)
    {
        yield return new WaitForSeconds(_time);
        VisitorCraete();
        if (container.QueueOfVisitors.HasFreePlace)
            CraeteVisitorAfter(interval);
    }

    private void VisitorCraete()
    {
        if (AllVisitorsCreated)                    
            return;         

        count++;
        Debug.Log("Входит " + count + " посетитель");
        GameObject _original = container.VisitorPrefab;
        Vector3 _position = doorPosition;
        Quaternion _rotation = Quaternion.identity;
        GameObject _visitorObject = Instantiate(_original, _position, _rotation);
        _visitorObject.name = count.ToString();
        container.QueueOfVisitors.VisitorEnqueue(_visitorObject);
        Visitor _visitorComponent = _visitorObject.GetComponent<Visitor>();
        _visitorComponent.SetContainer(container);   
    }

    private void SetTimer()
    {
        int _time = container.JsonSaver.Time;
        container.Timer.SetTime(_time);
    }

    private Vector3 doorPosition
    {
        get
        {
            int _rnd = Random.Range(0, container.DoorTransforms.Length);
            return container.DoorTransforms[_rnd].position;
        }
    }

    private bool AllVisitorsCreated
    {
        get 
        { 
            return count == container.JsonSaver.NumberOfVisitors; 
        }        
    }
}
