﻿using UnityEngine;

public class SpriteRendererRandomColor : MonoBehaviour
{    
    [SerializeField] private SpriteRenderer thisSpriteRenderer = null;

    private void Start()
    {
        SetColorRandom();
    }

    private void SetColorRandom()
    {
        float _r = Random.value;
        float _g = Random.value;
        float _b = Random.value;
        Color _color = new Color(_r, _g, _b);
        thisSpriteRenderer.color = _color;
    }
}
