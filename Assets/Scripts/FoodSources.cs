﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSources : MonoBehaviour
{
    [SerializeField] private Container container = null;
    [SerializeField] private GameObject[] foodSources = null;

    private void Start()
    {
        SetFoodSources();
    }

    private void SetFoodSources()
    {
        for (int _i = 0; _i < foodSources.Length; _i++)
        {
            bool _value = _i < container.JsonSaver.NumberOfFoodSources;
            foodSources[_i].SetActive(_value);
        }
    }
}
