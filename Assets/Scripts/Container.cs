﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : MonoBehaviour
{
    public JsonSaver JsonSaver = null;
    public QueueOfVisitors QueueOfVisitors = null;
    public VisitorsGenerator VisitorsGenerator = null;
    public GameObject VisitorPrefab = null;
    public Timer Timer = null;
    public Table Table = null;
    public Config Config = null;
    public Transform[] DoorTransforms = null;
}
