﻿using UnityEngine;

public class WindowLose : Window
{
    [SerializeField] private Container container = null;

    private void Start()
    {
        container.Timer.OnTimeEnd += OnTimeEnd;
    }

    private void OnDestroy()
    {
        container.Timer.OnTimeEnd -= OnTimeEnd;
    }

    private void OnTimeEnd()
    {        
        Show();
    }
}
