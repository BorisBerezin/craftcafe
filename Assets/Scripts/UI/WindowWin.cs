﻿using UnityEngine;

public class WindowWin : Window
{
    [SerializeField] private Container container = null;

    private void Start()
    {
        container.QueueOfVisitors.Win += Win;
    }

    private void OnDestroy()
    {
        container.QueueOfVisitors.Win -= Win;
    }

    private void Win()
    {        
        Show();
    }
}
