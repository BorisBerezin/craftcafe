﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Visitors : MonoBehaviour
{
    [SerializeField] private Container container = null;
    [SerializeField] private Text indicator = null;
    [SerializeField] private int completedOrders = 0;

    void Start()
    {
        container.Table.OrderCompleted += OnOrderCompleted;
        SetIndicator();
    }

    void OnDestroy()
    {
        container.Table.OrderCompleted -= OnOrderCompleted;
    }
    
    void OnOrderCompleted()
    {        
        completedOrders++;
        SetIndicator();
    }

    void SetIndicator()
    {
        string _numberOfVisitors = container.Config.NumberOfVisitors.ToString();
        string _completedOrders = completedOrders.ToString();
        string _text = _completedOrders + "/" + _numberOfVisitors;
        indicator.text = _text;
    }
}
