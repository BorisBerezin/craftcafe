﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonRestart : MonoBehaviour
{    
    public void Press()
    {
        Restart();
    }

    private void Restart()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
