﻿using UnityEngine;

public class Window : MonoBehaviour
{
    [SerializeField] private GameObject window = null;

    public void Show()
    {
        window.SetActive(true);
    }

    public void Hide()
    {
        window.SetActive(false);
    }
}
