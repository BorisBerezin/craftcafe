﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class QueueOfVisitors : MonoBehaviour
{
    [SerializeField] private Container container = null; 
    [SerializeField] private Transform[] queuePositions;    
    [SerializeField] private List<GameObject> queueList = new List<GameObject>();
    public event Action Win;

    public void VisitorEnqueue(GameObject _visitorObject)
    { 
        queueList.Add(_visitorObject);
    }

    public void VisitorDequeue(GameObject _visitorObject)
    {        
        queueList.Remove(_visitorObject);
        if (queueList.Count == 0)
        {
            Win?.Invoke();            
            Debug.LogWarning("Уровень пройден"); 
        }
    }

    public GameObject FirstVisitor
    {
        get
        {
            return queueList[0];            
        }
    }

    public Transform FreePlace
    {
        get
        {
            return queuePositions[queueList.Count - 1];
        }
    }

    public Transform NextPlace
    {
        get
        {
            if (queueList.Count == 0)
                return null;

            return queuePositions[queueList.Count - 1];
        }
    }

    public bool HasFreePlace
    {
        get
        {            
            
            return queueList.Count < queuePositions.Length;
        }        
    }

    public void MoveQueue()
    {        
        for (int _i = 0; _i < queueList.Count; _i++)
        {            
            queueList[_i].GetComponent<Visitor>().StartMoveTo(queuePositions[_i]);
        }        
        container.VisitorsGenerator.CraeteVisitorAfter(0);
    }
}
