﻿using System;
using System.IO;
using UnityEngine;
[Serializable] public struct Level
{
    public int NumberOfVisitors;
    public int NumberOfFoodSources;
    public int MaxFoodsInOrder;
    public int Time;    

    public Level(int _numberOfVisitors, int _numberOfFoodSources, int _maxFoodsInOrder, int _time)
    {
        NumberOfVisitors = _numberOfVisitors;
        NumberOfFoodSources = _numberOfFoodSources;
        MaxFoodsInOrder = _maxFoodsInOrder;
        Time = _time;
    }
}

public class JsonSaver : MonoBehaviour
{    
    [SerializeField] private Container container = null;
    private Level levelData = new Level();

    public int NumberOfVisitors
    {
        get
        {
            return levelData.NumberOfVisitors;
        }
    }

    public int NumberOfFoodSources
    {
        get
        {
            return levelData.NumberOfFoodSources;
        }
    }

    public int Time
    {
        get
        {
            return levelData.Time;
        }
    }

    private void Awake()
    {
        LoadLevelParamsFromConfig(); 
        ExportFileToJson(levelData);
    }

    public void ExportFileToJson(Level _levelData)
    {
        _levelData = new Level(_levelData.NumberOfVisitors, _levelData.NumberOfFoodSources, _levelData.MaxFoodsInOrder, _levelData.Time);
        string _contents = JsonUtility.ToJson(_levelData);
        string _path = Application.persistentDataPath + "/leveldata.json";
        File.WriteAllText(_path, _contents);
        Debug.LogWarning("Level settings exprted as: " + _path);
    }    

    public void ReadFromJson()
    {
        string _path = Application.persistentDataPath + "/leveldata.json";
        if (File.Exists(_path))
        {            
            string _json = File.ReadAllText(_path);
            levelData = JsonUtility.FromJson<Level>(_json);
        }
    }

    private void LoadLevelParamsFromConfig()
    {
        int _timer = container.Config.Timer;
        int _numberOfVisitors = container.Config.NumberOfVisitors;
        int _numberOfFoodSources = container.Config.NumberOfFoodSources;
        int _maxFoodsInOrder = container.Config.MaxFoodsInOrder;
        levelData = new Level(_numberOfVisitors, _numberOfFoodSources, _maxFoodsInOrder, _timer);
    }
}
