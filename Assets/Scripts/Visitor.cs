﻿using UnityEngine;
using UnityEngine.UI;


public class Visitor : MonoBehaviour
{
    [SerializeField] private Container container = null;
    [SerializeField] private Food[] foods;
    [SerializeField] private Image[] spriteRenderers = null;    

    private void Start()
    {
        SetFoodsRandom();
        Transform _finish = container.QueueOfVisitors.FreePlace;
        StartMoveTo(_finish);
    }

    public void ReciveFood(int _foodId)
    {
        bool _success = false;
        for (int _i = 0; _i < foods.Length; _i++)
        {            
            if (foods[_i].Id == _foodId && !foods[_i].Recived)
            {
                foods[_i].Recived = true;                
                foods[_i].Image.color = new Color32(1, 1, 1, 0);
                _success = true;                
                Debug.LogWarning("Посетитель " + gameObject.name + ". Получено блюдо " + _foodId);
                break;   
            }
        }  
        if (!_success)
            Debug.LogWarning("Посетитель " + gameObject.name + " не просил блюдо " + _foodId);

        if (IsOrderCompleted)
        {
            Debug.LogWarning("Посетитель " + gameObject.name + ". Заказ получен полностью");
            container.QueueOfVisitors.VisitorDequeue(gameObject);
            container.QueueOfVisitors.MoveQueue();
            container.Table.OnOrderCompleted();
            Destroy(gameObject);
        }          
    }

    public void SetContainer(Container _container)
    {
        container = _container;
    }

    public void StartMoveTo(Transform _finish)
    {
        MoveTo _visitorMoveTo = gameObject.AddComponent<MoveTo>();
        float _time = 1;
        _visitorMoveTo.StartMoveTo(_finish, _time);
    }

    private bool IsOrderCompleted
    {
        get
        {            
            for (int _i = 0; _i < foods.Length; _i++)
            {
                if (!foods[_i].Recived)                                    
                    return false;                                
            }
            return true;
        }
    }

    private void SetFoodsRandom()
    {
        int _maxFoodsInOrder = container.Config.MaxFoodsInOrder;
        int _foodsCount = 1 + Random.Range(0, _maxFoodsInOrder);
        foods = new Food[_foodsCount];
        for (int _i = 0; _i < foods.Length; _i++)
        {
            foods[_i] = new Food();
            int _foodId = Random.Range(0, container.JsonSaver.NumberOfFoodSources);
            foods[_i].Id = _foodId;
            foods[_i].Image = spriteRenderers[_i];
            foods[_i].Image.color = FoodColor(_foodId); 
        }
    }

    private Color FoodColor(int _foodId)
    {
        if (_foodId == 0)
            return Color.yellow;
        if (_foodId == 1)
            return Color.red;
        if (_foodId == 2)
            return Color.green;
        if (_foodId == 3)
            return Color.blue;
        if (_foodId == 4)
            return new Color(1, 1, 0, 1);
        if (_foodId == 5)
            return new Color(1, 0, 1, 1);
        if (_foodId == 6)
            return new Color(0, 1, 1, 1);
        if (_foodId == 7)
            return new Color(1, 1, 1, 1);
        return Color.black;
    }    
}
