﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTo : MonoBehaviour
{    
    private Transform finish = null;
    private Vector3 startPosition = Vector3.zero;    
    float progress = 0;
    bool isMoving = false;    
    private float time = 1.5f;

    public MoveTo(Transform _finish, float _time)
    {
        finish = _finish;
        time = _time;
    }

    void FixedUpdate()
    {
        if (isMoving)
        {            
            progress += Time.fixedDeltaTime / time;            
            transform.position = Vector3.Lerp(startPosition, finish.position, progress);
            if (progress > 1)
            {
                isMoving = false;
                Finish();
            }
        }
    }

    public void StartMoveTo(Transform _finish, float _time)
    {
        if (isMoving)
            return;

        startPosition = transform.position;
        finish = _finish;
        time = _time;
        isMoving = true;
        progress = 0;
    }

    void Finish()
    {
        Destroy(this);
    }
}
