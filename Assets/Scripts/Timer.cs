﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Container container = null;
    [SerializeField] private Text indicator = null;
    public event Action OnTimeEnd;
    float time = 0;

    private void Start()
    {
        container.QueueOfVisitors.Win += Win;
    }

    private void OnDestroy()
    {
        container.QueueOfVisitors.Win -= Win;
    }

    private void Win()
    {
        StopAllCoroutines();
    }


    public void SetTime(int _time)
    {
        time = _time;
        UpdateIndicator();
        StartCoroutine(WaitSecond());
    }

    IEnumerator WaitSecond()
    {
        yield return new WaitForSeconds(1);
        if (time > 0)
        { 
            time -= 1;
            StartCoroutine(WaitSecond());
        }
        UpdateIndicator();
        if (time == 0)
        {            
            OnTimeEnd?.Invoke();            
        }
    }

    private void UpdateIndicator()
    {
        indicator.text = TimeToString(time);
    }

    private string TimeToString(float _time)
    {
        int _min = Mathf.FloorToInt(_time/60);
        float _sec = _time - _min * 60;
        string _minStr = _min.ToString();
        string _secStr = _sec.ToString("f0");

        if (_secStr == "60")
            _secStr = "59";

        if (_secStr.Length == 1)
            _secStr = "0" + _secStr;

        return _minStr + ":" + _secStr;        
    }
}
