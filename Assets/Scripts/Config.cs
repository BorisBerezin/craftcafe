﻿using UnityEngine;

[CreateAssetMenu(fileName = "Config", menuName = "Config")]
public class Config : ScriptableObject
{
    [Header("JSON НАСТРОЙКИ")]      

    [Tooltip("Количество посетителей")]
    public int NumberOfVisitors = 8;

    [Tooltip("Количество блюд")]
    [Range(0, 8)]
    public int NumberOfFoodSources = 3;

    [Tooltip("Макс количество блюд в заказе")]
    [Range(0, 8)]
    public int MaxFoodsInOrder = 3;

    [Tooltip("Время прохождения уровня (сек)")]
    public int Timer = 7;    
}
